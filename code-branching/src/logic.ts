export enum Color {
  RED = 'Red',
  GREEN = 'Green',
  BLUE = 'Blue',
  WHITE = 'White',
  BLACK = 'Black',
  CYAN = 'Cyan',
  MAGENTA = 'Magenta',
  YELLOW = 'Yellow',
}

export function decideIfElse(color: Color | null): string {
  if (color === Color.WHITE) {
    return 'The brightest of them all!';
  } else if (
    [Color.CYAN, Color.YELLOW, Color.MAGENTA, Color.BLACK].includes(color)
  ) {
    return 'CMYK';
  } else if ([Color.RED, Color.GREEN, Color.BLUE].includes(color)) {
    return 'RGB';
  } else {
    return 'Chaos reigns this land!';
  }
}

export function decideSwitchVersionA(color: Color | null): string {
  switch (color) {
    case Color.WHITE:
      return 'The brightest of them all!';

    case Color.CYAN:
    case Color.YELLOW:
    case Color.MAGENTA:
    case Color.BLACK:
      return 'CMYK';

    case Color.RED:
    case Color.GREEN:
    case Color.BLUE:
      return 'RGB';

    default:
      return 'Chaos reigns this land!';
  }
}

export function decideElvis(color: Color | null): string {
  return color === Color.WHITE
    ? 'The brightest of them all!'
    : [Color.CYAN, Color.YELLOW, Color.MAGENTA, Color.BLACK].includes(color)
    ? 'CMYK'
    : [Color.RED, Color.GREEN, Color.BLUE].includes(color)
    ? 'RGB'
    : 'Chaos reigns this land!';
}

export function decideSwitchVersionB(color: Color | null): string {
  switch (true) {
    case color === Color.WHITE:
      return 'The brightest of them all!';

    case [Color.CYAN, Color.YELLOW, Color.MAGENTA, Color.BLACK].includes(color):
      return 'CMYK';

    case [Color.RED, Color.GREEN, Color.BLUE].includes(color):
      return 'RGB';

    default:
      return 'Chaos reigns this land!';
  }
}

const colorMapA = {
  [Color.WHITE]: 'The brightest of them all!',

  [Color.CYAN]: 'CMYK',
  [Color.YELLOW]: 'CMYK',
  [Color.MAGENTA]: 'CMYK',
  [Color.BLACK]: 'CMYK',

  [Color.RED]: 'RGB',
  [Color.GREEN]: 'RGB',
  [Color.BLUE]: 'RGB',
};

export function decideSwitchVersionC(color: Color | null): string {
  return colorMapA[color] ?? 'Chaos reigns this land!';
}

const colorMapB = new Map([
  [Color.WHITE, 'The brightest of them all!'],

  [Color.CYAN, 'CMYK'],
  [Color.YELLOW, 'CMYK'],
  [Color.MAGENTA, 'CMYK'],
  [Color.BLACK, 'CMYK'],

  [Color.RED, 'RGB'],
  [Color.GREEN, 'RGB'],
  [Color.BLUE, 'RGB'],
]);

export function decideMap(color: Color | null): string {
  return colorMapB.get(color) ?? 'Chaos reigns this land!';
}

// OOP Stuff
interface CanDecide {
  decide(color: Color | null): string | null;
}

class CMYK implements CanDecide {
  private allowedColors = [
    Color.CYAN,
    Color.YELLOW,
    Color.MAGENTA,
    Color.BLACK,
  ];

  public decide(color: Color | null): string | null {
    return this.allowedColors.includes(color) ? 'CMYK' : null;
  }
}

class BrightestOfThemAll implements CanDecide {
  public decide(color: Color | null): string | null {
    return Color.WHITE === color ? 'The brightest of them all!' : null;
  }
}

class RGB implements CanDecide {
  private allowedColors = [Color.RED, Color.GREEN, Color.BLUE];

  public decide(color: Color | null): string | null {
    return this.allowedColors.includes(color) ? 'RGB' : null;
  }
}

type Constructor<T> = new (...args: any[]) => T;

// Strategy OOP Pattern
class StrategyOOP {
  private deciders: CanDecide[] = [];

  // Factory method
  public use(deciderCtor: Constructor<CanDecide>): StrategyOOP {
    this.deciders.push(new deciderCtor());
    return this; // Builder pattern
  }

  public decide(color: Color | null): string {
    let result: string;
    this.deciders.some((decider) => {
      result = decider.decide(color);
      return result;
    });
    return result || 'Chaos reigns this land!';
  }
}

export function decideStrategyOOP(color: Color | null): string {
  return (
    new StrategyOOP()
      .use(BrightestOfThemAll)
      .use(CMYK)
      .use(RGB)
      .decide(color) || 'Chaos reigns this land!'
  );
}

// Functional Programming :D
type Predicate<T, V> = [(v: V) => boolean, T];
type GenericFunction<T, V> = (x: V) => T;

const head = (x: any[]) => x[0];
const tail = (x: any[]) => x.slice(1);

const cond =
  <T, V>(preds: Predicate<T, V>[]): GenericFunction<T, V> =>
  (x: V) =>
  head(preds)[0](x) ? head(preds)[1] : cond(tail(preds))(x);

const isBrightestOfThemAll = (c: Color | null) => c === Color.WHITE;
const isCMYK = (c: Color | null) =>
  [Color.CYAN, Color.YELLOW, Color.MAGENTA, Color.BLACK].includes(c);
const isRGB = (c: Color | null) =>
  [Color.RED, Color.GREEN, Color.BLUE].includes(c);
const always = (b: boolean) => (...args: any[]) => b;

export function decideFPRecurcive(color: Color | null): string {
  return cond<string, Color>([
    [isBrightestOfThemAll, 'The brightest of them all!'],
    [isCMYK, 'CMYK'],
    [isRGB, 'RGB'],
    [always(true), 'Chaos reigns this land!'],
  ])(color);
}
