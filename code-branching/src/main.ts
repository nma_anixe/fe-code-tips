import {
  Color, decideElvis, decideFPRecurcive, decideIfElse, decideStrategyOOP, decideSwitchVersionA,
  decideSwitchVersionB, decideSwitchVersionC
} from './logic';
import './style.css';

const app = document.querySelector<HTMLDivElement>("#app")!;

app.innerHTML = `
  <h1>Code Branching equals Decision Making!</h1>
  <i>... check your console log ...</i>
`;

const expected = {
  RED: "RGB",
  GREEN: "RGB",
  BLUE: "RGB",
  WHITE: "The brightest of them all!",
  BLACK: "CMYK",
  CYAN: "CMYK",
  MAGENTA: "CMYK",
  YELLOW: "CMYK",
};

[
  decideIfElse,
  decideElvis,
  decideSwitchVersionA,
  decideSwitchVersionB,
  decideSwitchVersionC,
  decideStrategyOOP,
  decideFPRecurcive,
].forEach((fn) => {
  console.group(`--- ${fn.name} ---`);
  Object.keys(Color).forEach((color) => {
    const result = fn(Color[color]) === expected[color];
    result
      ? console.log(`${color} is ${expected[color]}`, "(✔)")
      : console.error(`${color} is ${expected[color]}`, "(✗)");
  });
  console.groupEnd();
});
